import axios from "axios";

const state = {
  employees: [],
  employee: {},
  loading: false
};

const getters = {
  employees: state => state.employees,
  loading: state => state.loading
};

const actions = {
  getEmployees({ commit }) {
    commit("setLoading", true);
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(({ data }) => {
        commit("getEmployees", data);
      })
      .finally(() => commit("setLoading", false));
  },
  updateEmployee({ commit }, employee) {
    const { id } = employee;
    axios
      .put(`https://jsonplaceholder.typicode.com/users/${id}`, employee)
      .then(({ data }) => commit("updateEmployee", data));
  }
};

const mutations = {
  getEmployees: (state, employees) => (state.employees = employees),
  setLoading: (state, isLoading) => (state.loading = isLoading),
  updateEmployee: (state, newEmployee) => {
    const index = state.employees.findIndex(
      employee => employee.id === newEmployee.id
    );
    if (index !== -1) {
      state.employees.splice(index, 1, newEmployee);
    }
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
