import Vue from "vue";
import VueRouter from "vue-router";
import EmployeesList from "@pages/EmployeesList";
import UpdateEmploee from "@pages/UpdateEmploee";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "index",
      redirect: "list"
    },
    {
      path: "/list",
      name: "employeesList",
      component: EmployeesList
    },
    {
      path: "/update/:id",
      name: "updateEmploee",
      component: UpdateEmploee
    }
  ]
});

export default router;
